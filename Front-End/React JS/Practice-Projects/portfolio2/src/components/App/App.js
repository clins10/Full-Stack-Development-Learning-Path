import './App.css';
import Navbar from '../Navbar/Navbar.js';
// import Home from '../Home/Home.js';
// import About from '../About/About.js';
import Contact from '../Contact/Contact.js';

function App() {
  return (
    <div>
      <Navbar />
      {/*<Home />*/}
      {/*<About />*/}
      <Contact />
    </div>
  );
}

export default App;
